# 構成
```
imlabor
├ htdocs/：公開用ファイル
├ src/：ソース
│  ├ js/
│  ├ nunjucks/
│  └ scss/
├ .browserslistrc
├ .editorconfig
├ .gitignore
├ .node-version
├ .stylelintrc
├ gulpfile.js
├ package-lock.json
├ package.json
└ README.md
```

# 環境構築
Node.js：10.16.0（※ `.node-version` に記載）

```
$ npm i
```

# gulp

## ビルド
```bash
$ gulp
```

### ソースマップ出力
```bash
$ gulp -d
```

## Watch
```bash
$ gulp watch
```
または
```bash
$ gulp w
```

### ソースマップ出力
```bash
$ gulp watch -d
```
または
```bash
$ gulp w -d
```

## HTMLテンプレ―ト
[Nunjucks](https://mozilla.github.io/nunjucks/) を使用しています。

下記ファイルに現状の解析タグをコメントアウト `{# #}` で入れております。  
そのまま使用する場合はコメントを外してコンパイルしてください。

```
/src/nunjucks/_partials/_head-start.njk
/src/nunjucks/_partials/_head-end.njk
/src/nunjucks/_partials/_body-start.njk
```


## SCSS
[RSCSS](https://qiita.com/kk6/items/760efba180ec526903db) + [FLOCSS](https://github.com/hiloki/flocss) で構成しています。

## Javascript
Webpack + jQuery です。  
[Javascript Standard Style](https://standardjs.com/) で記載しているのでセミコロンは省略しています。  
guidepopup で表示するHTMLは `/src/js/modules/guidePopup.js` に記載されています。  
