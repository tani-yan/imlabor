// Util
const gulp = require('gulp')
const path = require('path')
const minimist = require('minimist')
const rename = require('gulp-rename')

// CSS/SCSS
const sass = require('gulp-sass')
const sassGlob = require('gulp-sass-glob')
const postcss = require('gulp-postcss')
const gulpStylelint = require('gulp-stylelint')

// WEBPACK
const webpack = require('webpack')
const gulpWebpack = require('webpack-stream')

// Nunjucks
const nunjucksRender = require('gulp-nunjucks-render')
const data = require('gulp-data')
const fs = require('fs')

// Browser-sync
const browserSync = require('browser-sync').create()

// Settings
const paths = {
  css: {
    src: './src/scss/**/*.scss',
    dest: './htdocs/'
  },
  js: {
    src: './src/js/**/*.js',
    dest: './htdocs/'
  },
  njk: {
    src: './src/nunjucks/**/*.njk',
    srcdir: './src/nunjucks/',
    data: './src/nunjucks/**/*.json',
    dest: './htdocs/'
  }
}

// 条件分岐
const options = minimist(process.argv.slice(2))
// const isDevelopment = (options.env === 'production') ? true : false;
const isDevelopment = (options.d) ? true : false;

// WEBPACK
const webpackOpts = {
  // Multi Files (with SplitChunks)
  // ==============================
  entry: {
    'js/common': './src/js/common.js'
  },
  output: {
    filename: '[name].bundle.js',
    path: path.join(__dirname, 'htdocs/')
  },
  optimization: {
    splitChunks: {
      name: 'js/vendor',
      chunks: 'initial'
    }
  },

  // Alias（package名をいれただけではimportできないファイルなどのエイリアスを設定）
  // ==============================
  resolve: {
    alias: {
    }
  },

  // Webpack Setting
  // ==============================
  watch: false,
  mode: (isDevelopment) ? 'development' : 'production',
  devtool: (isDevelopment) ? 'inline-source-map' : '',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: '/node_modules/lodash/',
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /jquery\.easing/,
        loader: 'imports-loader?define=>undefined'
        // Disable AMD => https://github.com/webpack-contrib/imports-loader#disable-amd
      }
    ]
  },

  plugins: [
    new webpack.ProvidePlugin({
      '$': 'jquery',
      'jQuery': 'jquery',
      'window.jQuery': 'jquery',
      'enquire': 'enquire.js',
      // 'is': 'is_js',
      // '_': 'lodash',
    }),
    // new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
  ],

  performance: {
    hints: (isDevelopment)  ? false : 'warning',
    maxEntrypointSize: 524288, // 500MB = 512*1024B
    maxAssetSize: 524288
  }
}

gulp.task('webpack', () => {
  return gulpWebpack(webpackOpts, webpack).on('error', function (e) {
    this.emit('end')
  })
    .pipe(gulp.dest(paths.js.dest))
})

// CSS
gulp.task('styles', done => {
  return gulp.src(paths.css.src)
    .pipe(sassGlob())
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(postcss([
      require('autoprefixer')(), // https://github.com/postcss/autoprefixer
      require('postcss-flexbugs-fixes')(), // https://github.com/luisrudge/postcss-flexbugs-fixes
      require('postcss-round-float')(4), // https://github.com/hail2u/postcss-round-float
      require('postcss-easing-gradients')(), // https://github.com/larsenwork/postcss-easing-gradients
      require('postcss-normalize-charset')(), // https://github.com/TrySound/postcss-normalize-charset
      require('css-mqpacker')() // https://github.com/hail2u/node-css-mqpacker
    ]))
    .pipe(gulpStylelint({
      failAfterError: false,
      fix: true,
      reporters: [// エラーを表示する場合はコメントを外す
      //  {formatter: 'string', console: true}
      ]
    }))
    .pipe(rename(path => {
      path.dirname = 'css/' + path.dirname
    }))
    .pipe(gulp.dest(paths.css.dest))
    done()
})

gulp.task('njk', () => {
  return gulp.src([ paths.njk.src, '!' + paths.njk.srcdir + '**/_*.njk' ])
    .pipe(data(() => {
      let locals = {
        'site': JSON.parse(fs.readFileSync(paths.njk.srcdir + '_data/site.json')),
        'articles': JSON.parse(fs.readFileSync(paths.njk.srcdir + '_data/articles.json')),
        'articles_jp': JSON.parse(fs.readFileSync(paths.njk.srcdir + '_data/articles_jp.json'))
      }
      return locals
    }))
    .pipe(data(file => {
      let absPath = file.path.split('\\').join('/')
      let relPath = absPath.split('nunjucks')[1].replace('.njk', '.html')
      relPath = relPath.replace(/index\.html$/, '')
      let info = '{"info": { "path": "' + relPath + '" }}'
      return JSON.parse(info)
    }))
    .pipe(nunjucksRender({
      path: ['src/nunjucks/']
    }))
    .pipe(gulp.dest(paths.njk.dest))
})

// Browser-sync
gulp.task('browser-sync', done => {
  browserSync.init({
    server: {
      baseDir: 'htdocs',
      index: 'index.html'
    }
  })
  done()
})

gulp.task('bs-reload', done => {
  browserSync.reload()
  done()
})

// WATCH
gulp.task('watch',
  gulp.series(gulp.parallel('styles', 'webpack', 'njk'), () => {
    gulp.watch([paths.njk.src, paths.njk.data], gulp.series('njk', 'bs-reload'))
    gulp.watch(paths.js.src, gulp.series('webpack', 'bs-reload'))
    gulp.watch(paths.css.src, gulp.series('styles', 'bs-reload'))
  })
)

// WATCH(SHORTCUT)
gulp.task('w', gulp.series('browser-sync', 'watch'))

// BUILD
gulp.task('build', gulp.parallel('styles', 'webpack', 'njk'))

// DEFAULT
gulp.task('default', gulp.task('build'))
