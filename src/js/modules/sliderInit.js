export default function sliderInit () {
  const $defaultSlider = $('.slider.-default')
  const $collapseSlider = $('.slider.-collapse')
  const $video = $('.slider .c-video')
  const $videoCaptionText = $('.c-video .caption').text()
  const $videoCaption = $('<p class="video-caption">' + $videoCaptionText + '</p>')
  const slickOptions = {
    variableWidth: true,
    centerMode: true,
    centerPadding: '15%',
    // dots: true,
    arrows: true,
    speed: 800,
    prevArrow: '<button class="slide-arrow prev-arrow"><span></span></button>',
    nextArrow: '<button class="slide-arrow next-arrow"><span></span></button>',
    responsive: [
      {
        breakpoint: 768,
        settings: {
          variableWidth: false,
          // centerMode: false,
          centerPadding: '5%',
          prevArrow: false,
          nextArrow: false,
          speed: 300
        }
      }
    ]
  }

  function counterInit (elm) {
    $(elm).not('.slick-initialized').each(function () {
      const $slideCounter = $('<div class="slick-counter"><span class="current"></span>of<span class="total"></span></div>')
      if (($('.video-caption').length)) {
        $('.video-caption').remove()
      }
      $(this).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        const $slideCaption = slick.$slides.eq(nextSlide).find('span').html()
        $('.current', this).html(nextSlide + 1)
        $('.slick-caption', this).html($slideCaption)
      }).on('breakpoint', function (event, slick, breakpoint) {
        $(this).append($slideCounter)
        $(this).append($('<div class="slick-caption"><span>' + $('.slick-current .caption', this).html() + '</span></div>'))
      }).on('init', function (event, slick) {
        if (!($('.slick-counter', this).length)) {
          $(this).append($slideCounter)
          $(this).append($('<div class="slick-caption"><span>' + $('.slick-current .caption', this).html() + '</span></div>'))
        }
        $('.current', this).html(slick.currentSlide + 1)
        $('.total', this).html(slick.slideCount)
      }).slick(slickOptions)
    })
  }

  counterInit($defaultSlider)
  if ($collapseSlider.length) {
    // $video.after($videoCaption)
    // console.log($videoCaption)
  }

  enquire.register('screen and (min-width: 1024px)', { // eslint-disable-line
    match: () => {
      counterInit($collapseSlider)
    },
    unmatch: () => {
      $collapseSlider.slick('unslick')
      if ($collapseSlider.length) {
        $video.after($videoCaption)
      }
    }
  })
}
