import Swiper from 'swiper'

export default function swiperInit () {
  if ($('.swiper-container').length > 0) {
    let contentSwiper = new Swiper ('.swiper-container', { // eslint-disable-line
      loop: true,
      speed: 800,
      autoplay: {
        delay: 6000,
        stopOnLast: false,
        disableOnInteraction: false
      }
    })
  }
}
