export default function menuActivate () {
  const $header = $('#l-header')
  const $drawerNav = $('#l-drawer-nav')
  const $drawerNavTrigger = $('#l-drawer-nav-toggle')
  const $main = $('#l-main')

  enquire.register('screen and (max-width: 1023px)', { // eslint-disable-line
    match: () => {
      let y = 0
      $drawerNavTrigger.on('click.gnav', (e) => {
        if ($header.hasClass('is-open')) {
          $drawerNavTrigger.text('MENU')
          $drawerNav.removeClass('is-open')
          $header.removeClass('is-open')
          $main.removeClass('is-alpha')
          // $drawerNav.fadeOut(300, () => {
          //   $header.removeClass('is-open')
          //   $drawerNavTrigger.removeClass('-close')
          // })
          $('body').removeAttr('style')
          $(window).scrollTop(y)
          // console.log('true')
        } else {
          $drawerNavTrigger.text('CLOSE')
          $drawerNav.addClass('is-open')
          $header.addClass('is-open')
          $main.addClass('is-alpha')
          // $drawerNav.fadeIn(300, () => {
          //   $header.addClass('is-open')
          // })
          y = $(window).scrollTop()
          $('body').css({
            position: 'fixed',
            top: -y + 'px',
            width: '100%'
          })
          // console.log('false')
        }
      })
    },
    unmatch: () => {
      $drawerNavTrigger.off('click.gnav')
      $header.removeClass('is-open')
      $drawerNavTrigger.removeClass('is-open')
      $main.removeClass('is-alpha')
      $drawerNav.removeAttr('style')
    }
  })
} // menuActivate
