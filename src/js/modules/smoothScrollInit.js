export default function smoothScrollInit() {
  const headerHeight = $(".l-header").height();

  $('a[href^="#"]').click(function () {
    const speed = 200;
    const href = $(this).attr("href");
    const target = $(href == "#" || href == "" ? "html" : href);
    const position = target.offset().top - headerHeight;
    $("body,html").animate({ scrollTop: position }, speed, "swing");
    // return false;
  });
}
