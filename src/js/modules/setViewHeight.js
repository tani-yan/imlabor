import enquire from 'enquire.js';
export default function setViewHeight () {

  enquire.register('screen and (max-width: 767px)', {
    match : () => {
      const kv = kv || {};
        kv.VIEW_HEIGHT = {
            init : function(){
                this.setParameters();
                this.setKvHeight();
                this.bind();
            },
            setParameters : function(){
                this.$window = $(window);
                // this.$target = $('#c-kv');
                // this.$target2 = $('.page-container');
                this.$target = $('.l-container');
            },
            bind : function(){
                const _self = this;
                this.$window.on('resize',function(){
                    _self.setKvHeight();
                });
            },
            setKvHeight : function(){
                // this.$target.css('height',this.$window.height()/3 * 2);
                // this.$target2.css('marginTop',this.$window.height()/3 * 2);
                this.$target.css('minHight',this.$window.height());
            }
        };
        $(function(){
            kv.VIEW_HEIGHT.init();
        });
    }
  })
} // setViewHeight
