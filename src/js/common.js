import "picturefill";
import "lazysizes";
// import 'lazysizes/plugins/unveilhooks/ls.unveilhooks.min.js'
import "jquery.easing";
import "waypoints/lib/jquery.waypoints.js";
import "slick-carousel";

// Functions
import menuActivate from "./modules/menuActivate";
import smoothScrollInit from "./modules/smoothScrollInit";
import scrollMenu from "./modules/scrollMenu";
import sliderInit from "./modules/sliderInit";

// TOP
// swiperInit()

$(function () {
  // COMMON
  menuActivate();
  smoothScrollInit();
  scrollMenu();
  sliderInit();
});
